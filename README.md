# General Introduction About API

### 1.1 Data Access API Introduction

The data access API exposes operations on application submitted data, also known as records.
This API is based on the REST Architectural style and all retreived data is returned in JSON format.
The data captured by application is stored in relational database. When accessing the data using the API, all security permissions as defined in the Access rules
for the application are enforced.

### 1.2 Necessary Requirements

- **PHP Version** - PHP 7.1 or PHP 7.0 - `ZipArchive`  extension.
