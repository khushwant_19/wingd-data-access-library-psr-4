<?php

namespace WingD;

use WingD\WingDAPI\Libraries\WingDImport;

/**
* WingDAPI by Erosteps
* version 1.2.8
* Last Update At 14th, November 2017
*/

/**
 * Class WingDAPI
 */
class WingDAPI extends WingDImport
{
  function __construct() {
    parent::__construct();
  }
}
