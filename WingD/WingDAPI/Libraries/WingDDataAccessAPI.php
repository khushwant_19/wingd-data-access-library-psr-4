<?php

namespace WingD\WingDAPI\Libraries;

class WingDDataAccessAPI
{
    // Init Curl Url
    private $url = 'https://api.wingd.co/index.php/api/request/';

    // Init Curl Request
    protected $WDDataAccess_init;

    // App Config
    protected $config = null;

    /**
     * WingDDataAccessAPI constructor.
     */
    public function __construct()
    {
        if ( session_status() == PHP_SESSION_NONE ) {
            session_start();
        }
        if ( isset($_SESSION['WDDataAccess_app_config']) ) {
            $this->config = json_decode($_SESSION['WDDataAccess_app_config']);
        }
    }

    /**
     * @param $creds_file
     * @return mixed
     */
    public function setAuth($creds_file = null)
    {
        try {
            if ($creds_file != null && file_exists( $creds_file )) {
                $creds = file_get_contents( $creds_file );
                $creds = json_decode( $creds );
                if (
                    (
                        ! empty($creds->app_id)
                        && gettype($creds->app_id) == "string"
                    )
                    && (
                        ! empty($creds->app_secret)
                        && gettype($creds->app_secret) == "string"
                    )
                    && (
                        ! empty($creds->app_secret)
                        && gettype($creds->app_encryption_key) == "string"
                    )
                ) {
                    $this->WDDataAccess_init("validateAppLogin");
                    $this->WDDataAccess_setHeaders(array(
                        "appid" => $creds->app_id,
                        "appsecretkey" => $creds->app_secret
                    ));
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1
                    ] );
                    $result = curl_exec( $this->WDDataAccess_init );
                    if ( json_decode( $result )->status ) {
                        $this->config = $creds;
                        $_SESSION['WDDataAccess_app_config'] = json_encode( $creds );
                    }
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('WingDDataAccessAPI::setAuth() Invalid Config File.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::setAuth() required 1 parameter a string. NULL given.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $url
     */
    protected function WDDataAccess_init($url = null)
    {
        try {
            if ( $url != null ) {
                $this->WDDataAccess_init = curl_init( $this->url . $url );
                if ( $this->haveAuth() ) {
                    $this->WDDataAccess_setHeaders( [
                        "appid" => $this->config->app_id,
                        "appsecretkey" => $this->config->app_secret
                    ] );
                }
            } else {
                throw new \Exception( 'Target Url parameter required to make instance of WingDDataAccessAPI.' );
            }
        } catch ( \Exception $e ) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     */
    protected function WDDataAccess_setHeaders( $arguments = null )
    {
        try {
            if ( $arguments != null ) {
                if (gettype($arguments) == 'array') {
                    $arguments = array_flip($arguments);
                    $arguments = array_map('strtolower', $arguments);
                    $arguments = array_flip($arguments);
                    $headers = array( "X-Forwarded-For: " . $this->getClientIp() );
                    foreach ( $arguments as $key => $value ) {
                        array_push( $headers, trim( $key ) . ': ' . trim( $value ) );
                        if ( array_key_exists( 'User-Agent', $arguments ) ) {
                            array_push( $headers, 'User-Agent: ' . $_SERVER['HTTP_USER_AGENT'] );
                        }
                    }
                    curl_setopt($this->WDDataAccess_init, CURLOPT_HTTPHEADER, $headers);
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::setHeaders() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::setHeaders() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    private function getClientIp()
    {
        $ipaddress = '';
        if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } elseif (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }
        return $ipaddress;
    }

    /**
     * @param null $initVar
     */
    protected function destroy($initVar = null)
    {
        if ($initVar != null) {
            curl_close($initVar);
        } else {
            curl_close($this->WDDataAccess_init);
        }
    }

    /**
     * @return bool
     */
    public function destroyAuth()
    {
        $this->config->app_id = null;
        $this->config->app_secret = null;
        if (isset($_SESSION['WDDataAccess_appId'])) {
            unset($_SESSION['WDDataAccess_appId']);
        }
        if (isset($_SESSION['WDDataAccess_appSecret'])) {
            unset($_SESSION['WDDataAccess_appSecret']);
        }
        if (
            $this->config->app_id === null
            && $this->config->app_secret === null
            && !isset($_SESSION['WDDataAccess_appId'])
            && !isset($_SESSION['WDDataAccess_appSecret'])
        ) {
            return true;
        } else {
            return false;
        }
    }

    // Content Functions Start

    /**
     * @param null $arguments
     * @return mixed
     */
    public function getContent($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("content?" . http_build_query($arguments));
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getContent() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::getContent() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return bool
     */
    public function haveAuth()
    {
        if (! is_object($this->config)) {
            return false;
        }
        return $this->config->app_id != null && $this->config->app_secret != null ? true : false;
    }

    public function getAppID()
    {
        try {
            if ($this->haveAuth()) {
                return $this->config->app_id;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function getContents($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("contents?" . http_build_query($arguments));
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getContents() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::getContents() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function createContent($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("content");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec( $this->WDDataAccess_init );
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createContent() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createContent() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function updateContent($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("content");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'PUT',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::updateContent() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::updateContent() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteContent($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("content");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteContent() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteContent() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    // Category Functions Start From Here

    /**
     * @param null $arguments
     * @return mixed
     */
    public function getCategory($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("category?" . http_build_query($arguments));
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getCategory() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::getCategory() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("categories");
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getSharedCategories()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("shared_app_categories");
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function createCategory($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("category");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createCategory() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createCategory() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function shareCategory($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("invite_user_for_category");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::shareCategory() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::shareCategory() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function updateCategory($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("category");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'PUT',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::updateCategory() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::updateCategory() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    // Relation Functions Start From Here

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteCategory($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("category");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteCategory() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteCategory() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getRelations()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("relationships");
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function createRelationName($arguments)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("relationshipname");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createRelationName() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createRelationName() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function createRelation($arguments)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("addrelationship");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createRelation() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createRelation() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function viewRelationship($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == "array") {
                        $this->WDDataAccess_init("relationshipdata?" . http_build_query($arguments));
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::viewRelationship() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::viewRelationship() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    // Person Access Functions

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteRelationship($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("relationship");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteRelationship() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteRelationship() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteRelationshipName($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("relationshipname");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteRelationship() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteRelationship() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function getPerson($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("person?" . http_build_query($arguments));
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getPerson() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::getPerson() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param array $arguments
     * @return mixed
     */
    public function getPersons($arguments = [])
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("persons?" . http_build_query($arguments));
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function createPerson($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("person");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createPerson() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createPerson() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function updatePerson($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("person");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'PUT',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::updatePerson() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::updatePerson() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deletePerson($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("person");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deletePerson() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deletePerson() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    // Location Functions

    /**
     * @param null $arguments
     * @return mixed
     */
    public function createLocation($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("location");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createLocation() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createLocation() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function getLocation($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("location?" . http_build_query($arguments));
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getLocation() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::getLocation() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getLocations()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("locations");
                curl_setopt_array($this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ]);
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function updateLocation($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("location");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'PUT',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::updateLocation() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::updateLocation() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteLocation($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("location");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteLocation() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteLocation() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("groupnames");
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function createGroupName($arguments)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("groupname");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createGroupName() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createGroupName() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function addGroupMember($arguments)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("groupmember");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::addGroupMember() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::addGroupMember() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function viewGroup($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == "array") {
                        $this->WDDataAccess_init("groupdata?" . http_build_query($arguments));
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::viewGroup() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::viewGroup() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteGroupMember($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("groupmember");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteGroupMember() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteGroupMember() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteGroupName($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("groupname");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteGroupName() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteGroupName() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    // Meta Data Functions

    /**
     * @param null $arguments
     * @return mixed
     */
    public function createMetaName($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("characteristic");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createMetaName() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createMetaName() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteMetaName($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("characteristic");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteMetaName() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteMetaName() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteMetaNames()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("characteristics");
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_CUSTOMREQUEST => 'DELETE'
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getMetaDataNames()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("characteristics");
                curl_setopt_array($this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ]);
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getMetaDataTypes()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("dataTypes");
                curl_setopt_array($this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ]);
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function insertMetaData($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("metadata");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::insertMetaData() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::insertMetaData() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function getMetaData($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("metadata?" . http_build_query($arguments));
                curl_setopt_array($this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ]);
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function updateMetaData($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("metadata");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'PUT',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::updateMetaData() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::updateMetaData() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteMetaData($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("metadata");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteMetaData() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteMetaData() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getHierarchies()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("hierarchies");
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function createHierarchyName($arguments)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("hierarchyname");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createRelationName() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createRelationName() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function createHierarchy($arguments)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("addhierarchy");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createHierarchy() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createHierarchy() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function viewHierarchy($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == "array") {
                        $this->WDDataAccess_init("hierarchydata?" . http_build_query($arguments));
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::viewHierarchy() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::viewHierarchy() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    // Person Access Functions

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteHierarchy($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("hierarchy");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteHierarchy() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteHierarchy() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public function deleteHierarchyName($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("hierarchyname");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CUSTOMREQUEST => 'DELETE',
                            CURLOPT_POSTFIELDS => http_build_query($arguments)
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteHierarchyName() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::deleteHierarchyName() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function wipeappdata()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("wipeappdata");
                curl_setopt_array($this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_CUSTOMREQUEST => 'DELETE',
                ]);
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function deleteApp()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("destroyapp");
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_CUSTOMREQUEST => 'DELETE',
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $media
     * @param $id
     * @return mixed
     */
    public function mediaUpload($media, $id)
    {
        try {
            if ($this->haveAuth()) {
                if ($media != null) {
                    if (gettype($media) == "array") {
                        if ($id != null) {
                            if (gettype($id) == "integer") {
                                $this->WDDataAccess_init("media");
                                curl_setopt_array( $this->WDDataAccess_init, [
                                    CURLOPT_POST => 2,
                                    CURLOPT_RETURNTRANSFER => 1,
                                    CURLOPT_POSTFIELDS => [
                                        "media" => new \CurlFile($media['tmp_name'], $media['type'], $media['name']),
                                        "id" => $id
                                    ]
                                ] );
                                $result = curl_exec($this->WDDataAccess_init);
                                $this->destroy();
                                return $result;
                            } else {
                                throw new \Exception('Argument 2 passed to WingDDataAccessAPI::mediaUpload() must be of the type integer, ' . gettype($id) . ' given.');
                            }
                        } else {
                            throw new \Exception('WingDDataAccessAPI::mediaUpload() required 2 parameter to be result.');
                        }
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::mediaUpload() must be of the type array, ' . gettype($media) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::mediaUpload() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $media
     * @param $id
     * @return mixed
     */
    public function mediaTransfer($media, $id)
    {
        try {
            if ($this->haveAuth()) {
                if ($media != null) {
                    if (gettype($media) == "array") {
                        if ($id != null) {
                            if (gettype($id) == "integer") {
                                $this->WDDataAccess_init("media");
                                curl_setopt_array( $this->WDDataAccess_init, [
                                    CURLOPT_POST => 2,
                                    CURLOPT_RETURNTRANSFER => 1,
                                    CURLOPT_POSTFIELDS => [
                                        "media" => new \CurlFile($media['path'], $media['type'], $media['name']),
                                        "id" => $id
                                    ]
                                ] );
                                $result = curl_exec($this->WDDataAccess_init) or die(curl_error($this->WDDataAccess_init));
                                $this->destroy();
                                return $result;
                            } else {
                                throw new \Exception('Argument 2 passed to WingDDataAccessAPI::mediaTransfer() must be of the type integer, ' . gettype($id) . ' given.');
                            }
                        } else {
                            throw new \Exception('WingDDataAccessAPI::mediaTransfer() required 2 parameter to be result.');
                        }
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::mediaTransfer() must be of the type array, ' . gettype($media) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::mediaTransfer() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function getMedia($id = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($id != null) {
                    if (gettype($id) == "integer") {
                        return $this->url . "media?id=" . $id;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getMedia() must be of the type integer, ' . gettype($id) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::getMedia() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function getUserProfileImage($username = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($username != null) {
                    if (gettype($username) == "string") {
                        return $this->url . "profilepic/" . $username;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getUserProfileImage() must be of the type string, ' . gettype($username) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::getUserProfileImage() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    final public function login($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        $this->WDDataAccess_init("login");
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init) or die(curl_error($this->WDDataAccess_init));
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::login() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::login() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param null $arguments
     * @return string
     */
    public function socialLoginUrl($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (gettype($arguments) == 'array') {
                        if (isset($arguments['provider']) && isset($arguments['redirect_success_uri'])) {
                            $arguments['app_id'] = $this->config->app_id;
                            urlencode($arguments['redirect_success_uri']);
                            if (
                                ! empty($this->config->providers->{$arguments['provider']})
                                && $this->config->providers->{$arguments['provider']}
                            ) {
                                $iv = bin2hex(
                                    mhash_keygen_s2k(
                                        MHASH_MD5,
                                        $this->config->app_id,
                                        $this->config->app_encryption_key,
                                        8
                                    )
                                );
                                $arguments['creds'] = bin2hex(
                                    openssl_encrypt(
                                        json_encode(
                                            $this->config->providers->{$arguments['provider']}
                                        ),
                                        'AES-128-CBC',
                                        $this->config->app_encryption_key,
                                        OPENSSL_RAW_DATA,
                                        $iv
                                    )
                                );
                            }
                            return $this->url . "socialLogin?" . http_build_query($arguments);
                        }
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::socialLoginUrl() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::socialLoginUrl() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function register($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("createAuthUser");
                    curl_setopt_array($this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_POST => count($arguments),
                        CURLOPT_POSTFIELDS => $arguments
                    ]);
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::register() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::register() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function checkUsername($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("checkUsername?" . http_build_query($arguments));
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::checkUsername() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::checkUsername() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function checkEmail($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("checkEmail?" . http_build_query($arguments));
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::checkEmail() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::checkEmail() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getApps($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (! empty($this->config->app_access_token)) {
                    if (gettype($arguments) == 'array') {
                        $default_arguments = [
                            'token' => $this->config->app_access_token
                        ];
                        $arguments = array_merge($default_arguments, $arguments);
                        $this->WDDataAccess_init("myapps?" . http_build_query($arguments));
                        curl_setopt_array( $this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getApps() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::getApps() required \'App Access Token\' For This Request.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::getApps() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getAppData()
    {
        try {
            if ($this->haveAuth()) {
                $this->WDDataAccess_init("appdata");
                curl_setopt_array($this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init) or die(curl_error($this->WDDataAccess_init));
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getAppStatistics()
    {
        try {
            $this->WDDataAccess_init("appstastics");
            curl_setopt_array( $this->WDDataAccess_init, [
                CURLOPT_RETURNTRANSFER => 1
            ] );
            $result = curl_exec($this->WDDataAccess_init);
            $this->destroy();
            return $result;
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getAppConfigs()
    {
        try {
            if (! empty($this->config->app_access_token)) {
                $arguments = [
                    'token' => $this->config->app_access_token
                ];
                $this->WDDataAccess_init("appconfig?" . http_build_query($arguments));
                curl_setopt_array($this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ]);
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('WingDDataAccessAPI::getAppConfigs() required \'App Access Token\' For This Request.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }
    /**
     * @param $arguments
     * @return mixed
     */
    public function getForms()
    {
        try {
            if (! empty($this->config->app_access_token)) {
                $arguments = [
                    'token' => $this->config->app_access_token
                ];
                $this->WDDataAccess_init("myforms?" . http_build_query($arguments));
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('WingDDataAccessAPI::getForms() required \'App Access Token\' For This Request.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getDiagrams($arguments = null)
    {
        try {
            if (! empty($this->config->app_access_token)) {
                $arguments = [
                    'token' => $this->config->app_access_token
                ];
                $this->WDDataAccess_init("mydiagrams?" . http_build_query($arguments));
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('WingDDataAccessAPI::getApps() required \'App Access Token\' For This Request.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getBuilders()
    {
        try {
            if (! empty($this->config->app_access_token)) {
                $arguments = [
                    'token' => $this->config->app_access_token
                ];
                $this->WDDataAccess_init("mybuilders?" . http_build_query($arguments));
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('WingDDataAccessAPI::getBuilders() required \'App Access Token\' For This Request.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getDiagramBuilders()
    {
        try {
            if (! empty($this->config->app_access_token)) {
                $arguments = [
                    'token' => $this->config->app_access_token
                ];
                $this->WDDataAccess_init("mydiagrambuilders?" . http_build_query($arguments));
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('WingDDataAccessAPI::getDiagramBuilders() required \'App Access Token\' For This Request.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getVideos()
    {
        try {
            if (! empty($this->config->app_access_token)) {
                $arguments = [
                    'token' => $this->config->app_access_token
                ];
                $this->WDDataAccess_init("myvideos?" . http_build_query($arguments));
                curl_setopt_array( $this->WDDataAccess_init, [
                    CURLOPT_RETURNTRANSFER => 1
                ] );
                $result = curl_exec($this->WDDataAccess_init);
                $this->destroy();
                return $result;
            } else {
                throw new \Exception('WingDDataAccessAPI::getVideos() required \'App Access Token\' For This Request.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getUseridByToken($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("myuserid?" . http_build_query($arguments));
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getUseridByToken() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::getUseridByToken() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getAuthProfile($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (! empty($this->config->app_access_token)) {
                    if (gettype($arguments) == 'array') {
                        $default_arguments = [
                            'token' => $this->config->app_access_token
                        ];
                        $arguments = array_merge($default_arguments, $arguments);
                        $this->WDDataAccess_init("getauthprofile?" . http_build_query($arguments));
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getAuthProfile() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::getAuthProfile() required \'App Access Token\' For This Request.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::getAuthProfile() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getProfile($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (! empty($this->config->app_access_token)) {
                    if (gettype($arguments) == 'array') {
                        $default_arguments = [
                            'token' => $this->config->app_access_token
                        ];
                        $arguments = array_merge($default_arguments, $arguments);
                        $this->WDDataAccess_init("profile?" . http_build_query($arguments));
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1
                        ]);
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getProfile() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::getProfile() required \'App Access Token\' For This Request.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::getProfile() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function updateAppConfig($arguments = null)
    {
        try {
            if ($this->haveAuth()) {
                if ($arguments != null) {
                    if (! empty($this->config->app_access_token)) {
                        if (gettype($arguments) == 'array') {
                            $default_arguments = [
                                'token' => $this->config->app_access_token
                            ];
                            $arguments = array_merge($default_arguments, $arguments);
                            $this->WDDataAccess_init("appconfig");
                            curl_setopt_array($this->WDDataAccess_init, [
                                CURLOPT_RETURNTRANSFER => 1,
                                CURLOPT_CUSTOMREQUEST => 'PUT',
                                CURLOPT_POSTFIELDS => http_build_query($arguments)
                            ]);
                            $result = curl_exec($this->WDDataAccess_init);
                            $this->destroy();
                            return $result;
                        } else {
                            throw new \Exception('Argument 1 passed to WingDDataAccessAPI::updateAppConfig() must be of the type array, ' . gettype($arguments) . ' given.');
                        }
                    } else {
                        throw new \Exception('WingDDataAccessAPI::updateAppConfig() required \'App Access Token\' For This Request.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::updateAppConfig() required 1 parameter to be result.');
                }
            } else {
                throw new \Exception('setAuth before Use Access Functions of WingD. Use setAuth(appid,appsecret).');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function createApp($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (! empty($this->config->app_access_token)) {
                    if (gettype($arguments) == 'array') {
                        $default_arguments = [
                            'token' => $this->config->app_access_token
                        ];
                        $arguments = array_merge($default_arguments, $arguments);
                        $this->WDDataAccess_init("createapp");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => count($arguments),
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::createApp() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createApp() required \'App Access Token\' For This Request.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::createApp() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getProfileByToken($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("myprofile?" . http_build_query($arguments));
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getProfileByToken() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::getProfileByToken() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function getProfileByUsername($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("profile?" . http_build_query($arguments));
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getProfileByUsername() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::getProfileByUsername() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function createAuthTokenByAccessToken($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (! empty($this->config->app_access_token)) {
                    if (gettype($arguments) == 'array') {
                        $default_arguments = [
                            'token' => $this->config->app_access_token
                        ];
                        $arguments = array_merge($default_arguments, $arguments);
                        $this->WDDataAccess_init("createauthtoken");
                        curl_setopt_array($this->WDDataAccess_init, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_POST => count($arguments),
                            CURLOPT_POSTFIELDS => $arguments
                        ] );
                        $result = curl_exec($this->WDDataAccess_init);
                        $this->destroy();
                        return $result;
                    } else {
                        throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getProfileByToken() must be of the type array, ' . gettype($arguments) . ' given.');
                    }
                } else {
                    throw new \Exception('WingDDataAccessAPI::createAuthTokenByAccessToken() required \'App Access Token\' For This Request.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::getProfileByToken() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function changePassword($arguments = null, $token = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("changeauthpassword");
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_CUSTOMREQUEST => 'PUT',
                        CURLOPT_POSTFIELDS => http_build_query($arguments)
                    ] );
                    $this->WDDataAccess_setHeaders( [
                        "token" => $token
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::changePassword() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::changePassword() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function forgotPassword($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("forgotauthpassword");
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_POST => count($arguments),
                        CURLOPT_POSTFIELDS => $arguments
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::forgotPassword() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::forgotPassword() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    /**
     * @param $arguments
     * @return mixed
     */
    public function resetPassword($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("resetauthpassword");
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_POST => count($arguments),
                        CURLOPT_POSTFIELDS => $arguments
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::resetPassword() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::resetPassword() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function validateAuthPassword($arguments)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("confirmauthpassword?" . http_build_query($arguments));
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::validateAuthPassword() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::validateAuthPassword() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function deleteAuthUser($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("deleteauthuser");
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_CUSTOMREQUEST => 'DELETE',
                        CURLOPT_POSTFIELDS => http_build_query($arguments)
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::deleteAuthUser() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::deleteAuthUser() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function logout($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("logout");
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_POST => count($arguments),
                        CURLOPT_POSTFIELDS => $arguments
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::logout() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::logout() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function shareApp($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("invite_user");
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_POST => count($arguments),
                        CURLOPT_POSTFIELDS => $arguments
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::shareApp() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::shareApp() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function unshareApp($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("unshare_app");
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_CUSTOMREQUEST => 'DELETE',
                        CURLOPT_POSTFIELDS => http_build_query($arguments)
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::unshareApp() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::unshareApp() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function unshareAppCategory($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("unshare_app_category");
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_CUSTOMREQUEST => 'DELETE',
                        CURLOPT_POSTFIELDS => http_build_query($arguments)
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::unshareAppCategory() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::unshareAppCategory() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function getAppSharedUsers()
    {
        try {
            $this->WDDataAccess_init("shared_app_users");
            curl_setopt_array( $this->WDDataAccess_init, [
                CURLOPT_RETURNTRANSFER => 1
            ] );
            $result = curl_exec($this->WDDataAccess_init);
            $this->destroy();
            return $result;
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }

    public function getAppsThatSharesCategory($arguments = null)
    {
        try {
            if ($arguments != null) {
                if (gettype($arguments) == 'array') {
                    $this->WDDataAccess_init("category_shared_apps?" . http_build_query($arguments));
                    curl_setopt_array( $this->WDDataAccess_init, [
                        CURLOPT_RETURNTRANSFER => 1
                    ] );
                    $result = curl_exec($this->WDDataAccess_init);
                    $this->destroy();
                    return $result;
                } else {
                    throw new \Exception('Argument 1 passed to WingDDataAccessAPI::getAppsThatSharesCategory() must be of the type array, ' . gettype($arguments) . ' given.');
                }
            } else {
                throw new \Exception('WingDDataAccessAPI::getAppsThatSharesCategory() required 1 parameter to be result.');
            }
        } catch (\Exception $e) {
            echo 'WingDDataAccessAPI Error : ' . $e->getMessage();
        }
    }
}
