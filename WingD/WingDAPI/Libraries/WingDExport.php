<?php

namespace WingD\WingDAPI\Libraries;

use WingD\WingDAPI\Libraries\WingDDataAccessAPI;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class WingDExport extends WingDDataAccessAPI
{
  private $PhpSpreadsheet = null;
  private $characteristics = [];

  public function __construct()
  {
    parent::__construct();
    $this->PhpSpreadsheet = new Spreadsheet();
  }

  private function get_php_excel_instance()
  {
    if (!empty($this->PhpSpreadsheet)) {
      return $this->PhpSpreadsheet;
    } else {
      return $this->PhpSpreadsheet = new Spreadsheet();
    }
  }

  public function exportData($export_data_types)
  {
    foreach ($export_data_types as $page => $data_type) {
      $data_type = strtolower($data_type);
      $this->PhpSpreadsheet = $this->get_php_excel_instance();
      $sheet = $this->PhpSpreadsheet->getSheetCount();
      $this->PhpSpreadsheet->setActiveSheetIndex($sheet - 1);
      $this->PhpSpreadsheet->getActiveSheet()->setTitle(strtoupper($data_type));
      $func = "export_" . $data_type;
      $res = $this->{$func}([]);

      if(!$res) {
        $this->PhpSpreadsheet->removeSheetByIndex($sheet - 1);
        $sheet = $this->PhpSpreadsheet->getSheetCount();
      }

      if (next($export_data_types)) {
        $this->PhpSpreadsheet->createSheet($sheet);
      } else {
        if (count($this->characteristics)) {
          $this->PhpSpreadsheet->createSheet($sheet);
          $this->PhpSpreadsheet->setActiveSheetIndex($sheet);
          $this->PhpSpreadsheet->getActiveSheet()->setTitle(strtoupper("Characteristics"));
          $this->createCharacteristicHeadingRows();
          $this->createCharacteristicRows($this->characteristics);
        }
      }
    }
    $fileName = implode('-', $export_data_types);
    if ($this->saveExcel($file)) {
      return [
        "status" => true,
        "download" => base_url("tmp/export/" . $fileName . "_" . $this->config->app_id . ".xlsx"),
        "fileName" => $fileName . "-" . date("M, Y") . ".xlsx",
        "path" => $file
      ];
    } else {
      return false;
    }
  }

  private function export_content_categories()
  {
    $data = json_decode($this->getCategories());
    if (!isset($data->error_code) && !empty($data)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createCategoryHeadingRows();
      $this->createCategoryRows($data, $row);
      return true;
    }
    return false;
  }

  private function export_contents()
  {
    $contents = json_decode($this->getContents([
      'bottom' => 0
    ]));
    if (!isset($contents->error_code) && !empty($contents)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createContentHeadingRows();
      $this->createContentRows($contents, $row);
      return true;
    }
    return false;
  }

  private function export_persons()
  {
    $data = json_decode($this->getPersons());
    if (!isset($data->error_code) && !empty($data)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createPersonHeadingRows();
      $this->createPersonRows($data, $row);
      return true;
    }
    return false;
  }

  private function export_locations()
  {
    $data = json_decode($this->getLocations());
    if (!isset($data->error_code) && !empty($data)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createLocationHeadingRows();
      $this->createLocationRows($data, $row);
      return true;
    }
    return false;
  }

  private function export_relationships()
  {
    $relationship_names = json_decode($this->getRelations());
    if (!isset($relationship_names->error_code) && !empty($relationship_names)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createRelationshipHeadingRows();
      foreach($relationship_names as $relationship_name) {
        $relationships = json_decode($this->viewRelationship([
          "name" => $relationship_name
        ]));
        if(!isset($relationships->error_code) && !empty($relationships)) {
          $row = $this->createRelationshipRows($relationships, $row);
        }
      }
      return true;
    }
    return false;
  }

  private function export_relationship_names()
  {
    $relationship_names = json_decode($this->getRelations());
    if (!isset($relationship_names->error_code) && !empty($relationship_names)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createCategoryHeadingRows();
      $this->createCategoryRows($relationship_names, $row);
      return true;
    }
    return false;
  }

  private function export_hierarchies()
  {
    $hierarch_names = json_decode($this->getHierarchies());
    if (!isset($hierarch_names->error_code) && !empty($hierarch_names)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createHierarchyHeadingRows();
      foreach($hierarch_names as $hierarch_name) {
        $hierarchies = json_decode($this->viewRelationship([
          "name" => $hierarch_name
        ]));
        if(!isset($hierarchies->error_code) && !empty($hierarchies)) {
          $row = $this->createHierarchyRows($hierarchies, $row);
        }
      }
      return true;
    }
    return false;
  }

  private function export_hierarchy_names()
  {
    $hierarch_names = json_decode($this->getHierarchies());
    if (!isset($hierarch_names->error_code) && !empty($hierarch_names)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createCategoryHeadingRows();
      $this->createCategoryRows($hierarch_names, $row);
      return true;
    }
    return false;
  }

  private function export_groups()
  {
    $group_names = json_decode($this->getGroups());
    if (!isset($group_names->error_code) && !empty($group_names)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createGroupHeadingRows();
      foreach($group_names as $group_name) {
        $groups = json_decode($this->viewGroup([
          "name" => $group_name
        ]));
        if(!isset($groups->error_code) && !empty($groups)) {
          $row = $this->createGroupRows($groups, $row);
        }
      }
      return true;
    }
    return false;
  }

  private function export_group_names()
  {
    $group_names = json_decode($this->getGroups());
    if (!isset($group_names->error_code) && !empty($group_names)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createCategoryHeadingRows();
      $this->createCategoryRows($group_names, $row);
      return true;
    }
    return false;
  }

  private function export_characteristic_names()
  {
    $characteristics = json_decode($this->getMetaDataNames());
    if (!isset($characteristics->error_code) && !empty($characteristics)) {
      $PHP_Excel_obj = $this->get_php_excel_instance();
      $row = $this->createCategoryHeadingRows();
      $this->createCategoryRows($characteristics, $row);
      return true;
    }
    return false;
  }

  private function createContentHeadingRows($row = 1)
  {
    $col = 0;
    $labels = [
      "S.No.",
      "ID",
      "name",
      "Category",
      "Description",
      "Rating"
    ];
    foreach ($labels as $label) {
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $label);
    }
    return $row + 1;
  }

  private function createPersonHeadingRows($row = 1)
  {
    $col = 0;
    $labels = [
      "S.No.",
      "ID",
      "First Name",
      "Middel Name",
      "Last Name",
      "Rating",
      "Locator String"
    ];
    foreach ($labels as $label) {
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $label);
    }
    return $row + 1;
  }

  private function createLocationHeadingRows($row = 1)
  {
    $col = 0;
    $labels = [
      "S.No.",
      "ID",
      "Name",
      "Latitude",
      "Longitude",
      "Elevation",
      "Address 1",
      "Address 2",
      "City",
      "State/Province/District",
      "Postal Code",
      "Country",
      "Rating"
    ];
    foreach ($labels as $label) {
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $label);
    }
    return $row + 1;
  }

  private function createCategoryHeadingRows($row = 1)
  {
    $col = 0;
    $labels = [
      "S.No.",
      "Name"
    ];
    foreach ($labels as $label) {
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $label);
    }
    return $row + 1;
  }

  private function createRelationshipHeadingRows($row = 1)
  {
    $col = 0;
    $labels = [
      "S.No.",
      "ID",
      "Name",
      "Parent",
      "Child",
      "Strength"
    ];
    foreach ($labels as $label) {
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $label);
    }
    return $row + 1;
  }

  private function createHierarchyHeadingRows($row = 1)
  {
    $col = 0;
    $labels = [
      "S.No.",
      "ID",
      "Name",
      "Parent",
      "Child",
      "Level"
    ];
    foreach ($labels as $label) {
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $label);
    }
    return $row + 1;
  }

  private function createGroupHeadingRows($row = 1)
  {
    $col = 0;
    $labels = [
      "S.No.",
      "ID",
      "Name",
      "Member"
    ];
    foreach ($labels as $label) {
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $label);
    }
    return $row + 1;
  }

  private function createCharacteristicHeadingRows($row = 1)
  {
    $col = 0;
    $labels = [
      "S.No.",
      "/Thing/ID",
      "Name",
      "Type",
      "Value"
    ];
    foreach ($labels as $label) {
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $label);
    }
    return $row + 1;
  }

  // Data Rows Function Start Here

  private function createContentRows($data, $row = 1, $export_characteristics = true)
  {
    $sno = 1;
    foreach ($data as $item) {
      $col = 0;
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $sno);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->id) ? $item->id : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->name) ? $item->name : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->category) ? $item->category : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->description) ? $item->description : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->rating) ? $item->rating : '');
      if ($export_characteristics && !empty($item->Characteristics)) {
        $uri = "/content/$item->id";
        foreach ($item->Characteristics as $characteristic) {
          $name_hash = md5($characteristic->name);
          $this->characteristics[$uri][$name_hash] = $characteristic;
        }
      }
      $row++;
      $sno++;
    }
    return $row;
  }

  private function createPersonRows($data, $row = 1, $export_characteristics = true)
  {
    $sno = 1;
    foreach ($data as $item) {
      $col = 0;
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $sno);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->id) ? $item->id : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->firstName) ? $item->firstName : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->middleName) ? $item->middleName : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->lastName) ? $item->lastName : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->rating) ? $item->rating : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->locatorString) ? $item->locatorString : '');
      if ($export_characteristics && !empty($item->Characteristics)) {
        $uri = "/person/$item->id";
        foreach ($item->Characteristics as $characteristic) {
          $name_hash = md5($characteristic->name);
          $this->characteristics[$uri][$name_hash] = $characteristic;
        }
      }
      $row++;
      $sno++;
    }
    return $row;
  }

  private function createLocationRows($data, $row = 1, $export_characteristics = true)
  {
    $sno = 1;
    foreach ($data as $item) {
      $col = 0;
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $sno);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->id) ? $item->id : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->name) ? $item->name : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->latitude) ? $item->latitude : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->longitude) ? $item->longitude : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->elevation) ? $item->elevation : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->address1) ? $item->address1 : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->address2) ? $item->address2 : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->city) ? $item->city : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->stateProvinceDistrict) ? $item->stateProvinceDistrict : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->postalCode) ? $item->postalCode : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->country) ? $item->country : '');
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, isset($item->rating) ? $item->rating : '');
      if ($export_characteristics && !empty($item->Characteristics)) {
        $uri = "/location/$item->id";
        foreach ($item->Characteristics as $characteristic) {
          $name_hash = md5($characteristic->name);
          $this->characteristics[$uri][$name_hash] = $characteristic;
        }
      }
      $row++;
      $sno++;
    }
    return $row;
  }

  private function createCategoryRows($data, $row = 1)
  {
    $sno = 1;
    foreach ($data as $item) {
      $col = 0;
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $sno);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item);
      $row++;
      $sno++;
    }
    return $row;
  }

  private function createRelationshipRows($data, $row = 1)
  {
    $sno = 1;
    foreach ($data as $item) {
      $col = 0;
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $sno);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->id);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $data[0]->name);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, "/" . $item->parent->type . "/" . $item->parent->id);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, "/" . $item->child->type . "/" . $item->child->id);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->strength);
      $row++;
      $sno++;
    }
    return $row;
  }

  private function createHierarchyRows($data, $row = 1)
  {
    $sno = 1;
    foreach ($data as $item) {
      $col = 0;
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $sno);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->id);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $data[0]->name);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, "/" . $item->parent->type . "/" . $item->parent->id);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, "/" . $item->child->type . "/" . $item->child->id);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->level);
      $row++;
      $sno++;
    }
    return $row;
  }

  private function createGroupRows($data, $row = 1)
  {
    $sno = 1;
    foreach ($data as $item) {
      $col = 0;
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $sno);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $item->id);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $data[0]->name);
      $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, "/" . $item->member->type . "/" . $item->member->id);
      $row++;
      $sno++;
    }
    return $row;
  }

  private function createCharacteristicRows($data, $row = 1)
  {
    $sno = 1;
    foreach ($data as $uri => $characteristics) {
      if (count($characteristics)) {
        foreach ($characteristics as $i => $characteristic) {
          $col = 0;
          $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $sno);
          $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $uri);
          $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $characteristic->name);
          $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $characteristic->dataType);
          $this->PhpSpreadsheet->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $characteristic->value);
          $row++;
          $sno++;
        }
      }
    }
    return $row;
  }

  private function get_url_argument_value($arg, $url)
  {
    $query = parse_url($url, PHP_URL_QUERY);
    $query_array = explode('&', $query);
    foreach ($query_array as $query) {
      list($key, $value) = explode('=', $query);
      if ($arg == $key) {
        return $value;
      } else {
        return false;
      }
    }
  }

  private function saveExcel($file)
  {
    try {
      $objWriter = new Xlsx($this->PhpSpreadsheet);
      $objWriter->save($file);
    } catch (\Exception $e) {
      return false;
    }
    return true;
  }
}
