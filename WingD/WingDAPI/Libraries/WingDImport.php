<?php

namespace WingD\WingDAPI\Libraries;

use WingD\WingDAPI\Libraries\WingDExport;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class WingDImport extends WingDExport {
  private $import_options = [];
  public function __construct()
  {
    parent::__construct();
  }
  public function importData($file, $fields, $options = [])
  {
    //  Read your Excel workbook
    try {
      $inputFileType = IOFactory::identify($file);
      $objReader = IOFactory::createReader($inputFileType);
      $objPHPSpreadSheet = $objReader->load($file);
    } catch(\Exception $e) {
      die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
    }
    $this->import_options = $options ? $options : [];
    $data = [];
    $totalSheets = $objPHPSpreadSheet->getSheetCount();
    for($currentSheet = 0; $currentSheet < $totalSheets; $currentSheet++) {
      $sheet = $objPHPSpreadSheet->getSheet($currentSheet);
      $sheetTitle = $sheet->getTitle();
      $highestRow = $sheet->getHighestDataRow();
      $highestColumn = $sheet->getHighestDataColumn();
      if(!in_array(strtolower($sheetTitle), $fields)) {
        continue;
      }
      switch (strtolower($sheetTitle)) {
        case "contents" :
          $attrs = [
            "n",
            "c",
            "d",
            "r"
          ];
          $idIndex = 1;
          $indexingStartRow = 2;
          $indexingEndRow = 5;
          break;
        case "persons" :
          $attrs = [
            "f",
            "m",
            "l",
            "r",
            "ls"
          ];
          $idIndex = 1;
          $indexingStartRow = 2;
          $indexingEndRow = 6;
          break;
        case "locations" :
          $attrs = [
            "n",
            "la",
            "lo",
            "el",
            "a1",
            "a2",
            "c",
            "st",
            "p",
            "co",
            "r"
          ];
          $idIndex = 1;
          $indexingStartRow = 2;
          $indexingEndRow = 12;
          break;
        case "relationships" :
          $attrs = [
            "n",
            "a",
            "b",
            "s"
          ];
          $indexingStartRow = 2;
          $indexingEndRow = 5;
          break;
        case "hierarchies" :
          $attrs = [
            "n",
            "p",
            "c",
            "l"
          ];
          $indexingStartRow = 2;
          $indexingEndRow = 5;
          break;
        case "groups" :
          $attrs = [
            "n",
            "t"
          ];
          $indexingStartRow = 2;
          $indexingEndRow = 3;
          break;
        case "content_categories" :
          $attrs = [
            "c"
          ];
          $indexingStartRow = 1;
          $indexingEndRow = 1;
          break;
        case "relationship_names" :
          $attrs = [
            "n"
          ];
          $indexingStartRow = 1;
          $indexingEndRow = 1;
          break;
        case "hierarchy_names" :
          $attrs = [
            "n"
          ];
          $indexingStartRow = 1;
          $indexingEndRow = 1;
          break;
        case "group_names" :
          $attrs = [
            "n"
          ];
          $indexingStartRow = 1;
          $indexingEndRow = 1;
          break;
        case "characteristic_names" :
          $attrs = [
            "c"
          ];
          $indexingStartRow = 1;
          $indexingEndRow = 1;
          break;
        case "characterstic" :
          $attrs = [
            "uri",
            "n",
            "t",
            "v"
          ];
          $indexingStartRow = 1;
          $indexingEndRow = 4;
          break;
        default :
          break;
      }
      //  Loop through each row of the worksheet in turn
      $rowStartIndex = 2;
      for ($row = $rowStartIndex; $row <= $highestRow; $row++) {
        //  Read a row of data into an array
        $rowData = $sheet->rangeToArray(
          'A' . $row . ':' . $highestColumn . $row,
          NULL,
          TRUE,
          FALSE
        );
        //  Insert row data array into your database of choice here
        for($i = $indexingStartRow; $i <= $indexingEndRow; $i++){
          if(isset($idIndex)) {
            $data[strtolower($sheetTitle)][$rowData[0][$idIndex]][$attrs[$i - $indexingStartRow]] = $rowData[0][$i];
          } else {
            $data[strtolower($sheetTitle)][$row - $rowStartIndex][$attrs[$i - $indexingStartRow]] = $rowData[0][$i];
          }
        }
      }
    }
    $file = $this->createMacroFile($this->importMacroBuilder($data));
    if($file) {
      $this->WDDataAccess_init("installapp");
      curl_setopt_array($this->WDDataAccess_init, array(
        CURLOPT_POST => 2,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POSTFIELDS => [
          "macro" => new \CurlFile(
            $file,
            "text/plain",
            "macro.txt"
          ),
          "appid" => $this->config->app_id
        ]
      ));
      $this->WDDataAccess_setHeaders([
        "appid" => $this->config->app_id
      ]);
      $result = curl_exec($this->WDDataAccess_init) or die(curl_error($this->WDDataAccess_init));
      $this->destroy();
      return $result;
    }
  }
  private function replacer($str, $changes = [])
  {
    foreach ($changes as $replace => $with) {
      $str = str_replace($replace, $with, $str);
    }
    return $str;
  }
  private function importMacroBuilder($data)
  {
    $macro_map = '';
    $uriParse = [
      "contents" => "content",
      "persons" => "person",
      "locations" => "location",
      "relationships" => "relationship",
      "content_categories" => "contentcategory",
      "relationship_names" => "relationshipname",
      "hierarchy_names" => "hierarchyname",
      "group_names" => "groupname",
      "characteristic_names" => "characteristic",
      "hierarchies" => "hierarchy",
      "groups" => "group",
    ];
    if(count($data)) {
      $data = $this->importprt($data);
      foreach ($data as $uri => $arrays) {
        foreach ($arrays as $id => $item) {
          if(in_array($uri, [ "contents", "persons", "locations", "calendars" ])) {
            if(
              $uri == 'contents' &&
              !empty($this->import_options['category_for_contents']) &&
              strtolower($this->import_options['category_for_contents']) != 'null'
            ) {
              $item['c'] = $this->import_options['category_for_contents'];
            }
            $macro_map .= "\$id{$uriParse[$uri]}{$id} = \$post /{$uriParse[$uri]} \"{$this->buildMacroAttrFromArray($item)}\"\n";
            switch ($uri) {
              case "contents":
                $rel_attr["n"] = "is created with userdata app";
                break;
              case "persons":
                $rel_attr["n"] = "person is created with userdata app";
                break;
              case "calendars":
                $rel_attr["n"] = "calendar is created with userdata app";
                break;
              case "locations":
                $rel_attr["n"] = "location is created with userdata app";
                break;
              default:
                break;
            }
            $rel_attr["a"] = "/{$uriParse[$uri]}/\$id{$uriParse[$uri]}{$id}";
            $rel_attr["b"] = "/content/{APP_ID_CONTENT_ID}";
            $rel_attr["s"] = 10;
            $macro_map .= "\$post /relationship \"{$this->buildMacroAttrFromArray($rel_attr)}\"\n";
          } else if(in_array($uri, [ "relationships", "hierarchies", "groups" ])) {
            switch ($uri) {
              case "relationships" :
                $a = explode('/', $item["a"]);
                if(array_key_exists($a[2], $data[$a[1] . "s"])) {
                  $item["a"] = "/{$a[1]}/\$id{$a[1]}{$a[2]}";
                }
                $b = explode('/', $item["b"]);
                if(array_key_exists($b[2], $data[$b[1] . "s"])) {
                  $item["b"] = "/{$b[1]}/\$id{$b[1]}{$b[2]}";
                }
                break;
              case "hierarchies" :
                $a = explode('/', $item["p"]);
                if(array_key_exists($a[2], $data[$a[1] . "s"])) {
                  $item["p"] = "/{$a[1]}/\$id{$a[1]}{$a[2]}";
                }
                $b = explode('/', $item["c"]);
                if(array_key_exists($b[2], $data[$b[1] . "s"])) {
                  $item["c"] = "/{$b[1]}/\$id{$b[1]}{$b[2]}";
                }
                break;
              case "groups" :
                $a = explode('/', $item["t"]);
                if(array_key_exists($a[2], $data[$a[1] . "s"])) {
                  $item["t"] = "/{$a[1]}/\$id{$a[1]}{$a[2]}";
                }
                break;
              default:
                break;
            }
            $macro_map .= "\$id = \$post /{$uriParse[$uri]} \"{$this->buildMacroAttrFromArray($item)}\"\n";
          } else if(in_array($uri, [ "characterstic" ])) {
            $a = explode('/', $item["uri"]);
            if(array_key_exists($a[2], $data[$a[1] . "s"])) {
              $item["uri"] = "/{$a[1]}/\$id{$a[1]}{$a[2]}";
            }
            $charUri = $item["uri"];
            unset($item["uri"]);
            $macro_map .= "\$post {$charUri}/characteristic \"{$this->buildMacroAttrFromArray($item)}\"\n";
          } else {
            switch ($uri)
            {
              case "content_categories":
                $content_info["c"] = "UserData Apps ContentCategory";
                $index = "c";
                break;
              case "relationship_names":
                $content_info["c"] = "UserData Apps RelationshipNames";
                $index = "n";
                break;
              case "hierarchy_names":
                $content_info["c"] = "UserData Apps HierarchyNames";
                $index = "n";
                break;
              case "group_names":
                $content_info["c"] = "UserData Apps GroupNames";
                $index = "n";
                break;
              case "characteristic_names":
                $content_info["c"] = "UserData Apps Characteristic";
                $index = "c";
                break;
              default:
                break;
            }
            if(isset($content_info)){
              $macro_map .= "\$post /{$uriParse[$uri]} \"{$this->buildMacroAttrFromArray($item)}\"\n";
              $content_info['n'] = $item[$index];
              $macro_map .= "\$CID = \$post /content \"{$this->buildMacroAttrFromArray($content_info)}\"\n";
              $CC_attr["n"] = "associatedappid";
              $CC_attr["t"] = "string";
              $CC_attr["v"] = "{APP_ID}";
              $macro_map .= "\$post /content/\$CID/characteristics \"{$this->buildMacroAttrFromArray($CC_attr)}\"\n";
            }
          }
        }
      }
    }
    return $macro_map;
  }
  private function buildMacroAttrFromArray($array)
  {
    $attrString = '';
    foreach ($array as $key => $value) {
      if(!empty($value)) {
        $value = $this->applyMacroAttrValueFilters($key, $value);
        $attrString .= $key . "=" . $value . ";";
      }
    }
    return $attrString;
  }
  private function importprt($data)
  {
    $pIndex = [
      "content_categories",
      "relationship_names",
      "hierarchy_names",
      "group_names",
      "characterstic_names",
      "contents",
      "persons",
      "locations",
      "relationships",
      "hierarchies",
      "groups",
      "characterstic"
    ];
    $newData = [];
    foreach ($pIndex as $i => $key) {
      if(isset($data[$key]) && count($data[$key])) {
        $newData[$key] = $data[$key];
      }
    }
    return $newData;
  }
  function createMacroFile($macro, $path = null)
  {
    try {
      if ($path === null) {
        $path = dirname(__FILE__) . "/tmp/";
      }
      if( !file_exists($path) ) mkdir( file_exists($path) );
      if (file_exists($path) && is_writable($path)) {
        $fileName = $path . "IMPORT_" . md5(time()) . ".txt";
        $file = fopen($fileName, "w");
        fwrite($file, $macro);
        fclose($file);
        return $fileName;
      } else {
        throw new \Exception("Path You Provided Is Not Valid. Check Path : $path is exist and writeable.");
      }
    } catch (\Exception $e) {
      echo 'WingDExport Error : ' . $e->getMessage();
    }
  }
  private function applyMacroAttrValueFilters($key, $value) {
    $ingnore_keys = [ 'id', 'a', 'b', 'p', 'c', 't' ];
    if(!in_array(strtolower($key), $ingnore_keys)) {
      $value = str_replace("=", "", $value); // Replacing =
      $value = str_replace("$", "", $value); // Replacing $
      $value = str_replace("\"", "", $value); // Replacing "
      $value = str_replace(":", "", $value); // Replacing :
      $value = str_replace("#", "", $value); // Replacing #
      $value = str_replace(";", "", $value); // Replacing ;
    }
    return $value;
  }
}
